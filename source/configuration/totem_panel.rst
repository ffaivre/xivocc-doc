************
Totem Panels
************

Disable Totem Panels
====================

If you do not use pannels and elasticsearch, it is possible to disable then.

* Set DISABLEELASTIC environment variable to true in docker-xivocc.yml section xivo_replic
* Remove elasticsearch section
* remove elasticsearch link in Nginx section
* add elasticsearch as extra_host to elacticsearch:127.0.0.1
