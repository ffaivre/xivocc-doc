########
Features
########

.. toctree::
   :maxdepth: 2

   ccmanager/ccmanager
   agent/agent
   config_mgt/index
   xivo_stats
   totem_pannels

