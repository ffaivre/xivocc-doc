.. _agent:

#################
Agent environment
#################

.. figure:: agent.png
    :scale: 80%

Web application for contact center agents

================
Configuration
================

Recording can be paused or started by an agent, this feature can be disabled by changing showRecordingControls option in application.conf,
you can also set the environnment variable SHOW_RECORDING_CONTROLS to false for your xucmgt container in docker compose yml file. When disabled the recording status is not displayed any more

.. figure:: agent_recording.png
    :scale: 90%


Callbacks panel can be removed using by changing showCallbacks option in application.conf,
you can also use SHOW_CALLBACKS environment variable in docker compose yml file.


By using the showQueueControls option in application.conf, you may allow an agent to enter or leave a queue.
You can also use SHOW_QUEUE_CONTROLS environment variable in docker compose yml file.

.. figure:: agent_queue.png
    :scale: 90%

.. _agent_callbacks:
	    
================
Taking Callbacks
================

The agent can see the :ref:`callbacks <callbacks>` related to the queues he is logged on.
They are available in the ``Callbacks`` tab, beside the ``Agents of my group`` tab.

On this page, the agent only has access to basic information about the callback: the phone number to call,
the person's name and its company name. On the left of each callback line, a colored clock indicates the temporal status of this callback:

- yellow if the callback is to be processed later
- green if we are currently inside the callback period
- red if the callback period is over

.. figure:: agent-callbacks-view.png
    :scale: 90%

To process one of these callbacks, the agent must click on one of the callbacks line. This will remove the callback from the other agents' list,
and trigger the following screen:

.. figure:: agent-callback-edit.png
    :scale: 90%

To launch the call, the agent must click on one of the available phone numbers.
Once the callback is launched, the status can be changed and a comment can be added.

If you set 'Callback' as status, the callback can be rescheduled at a later time and another period:

.. figure:: agent-callback-reschedule.png
    :scale: 90%

Clicking on the calendar icon next to the "New due date" field, will popup a calendar to select another callback date.

============
Screen Popup
============


It is possible to display customer information in an external web application using Xivo sheet_ mecanism.

.. _sheet: http://documentation.xivo.fr/en/stable/administration/customerinfo/sheetconfiguration.html#sheets

You must define a sheet with two fields

* folderNumber
    have to be defined. Can be calculated or use a default value not equal to "-"
* popupUrl
    The url to open when call arrives : i.e. http://mycrm.com/customerInfo?folder= the folder number will be automatically appended to
    the end of the URL

Example : Using the caller number to open a customer info web page

    * Define folderNumber with any default value i.e. 123456
    * Define popupUrl with a display value of http://mycrm.com/customerInfo?nb={xivo-calleridnum}&fn= when call arrives web page http://mycrm.com/customerInfo?nb=1050&fn=123456 will be displayed


